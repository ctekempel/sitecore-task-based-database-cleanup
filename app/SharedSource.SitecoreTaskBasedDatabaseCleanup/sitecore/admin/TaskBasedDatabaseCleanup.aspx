﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskBasedDatabaseCleanup.aspx.cs" Inherits="SharedSource.SitecoreTaskBasedDatabaseCleanup.Sitecore.Admin.TaskBasedDatabaseCleanup" %>

<%@ Import Namespace="System.Web.DynamicData" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.UI.WebControls.Expressions" %>
<%@ Import Namespace="System.Web.UI.WebControls.WebParts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div>
            <h3>Databases</h3>
            <asp:CheckBoxList runat="server" ID="databaseList" />
        </div>
        <div>
            <h3>Tasks</h3>
            <asp:CheckBoxList runat="server" ID="taskList" />
        </div>
        <asp:Button runat="server" ID="run" OnClick="RunOnClick" Text="Run Clean-Up" />

        <asp:UpdatePanel runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="logRefreshTimer"/>
            </Triggers>
            <ContentTemplate>
                <div>
                    <h3>Status</h3>
                    <asp:TextBox runat="server" ID="log" TextMode="MultiLine" Rows="50" Columns="200"></asp:TextBox>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:Timer runat="server" Interval="5000" ID="logRefreshTimer"></asp:Timer>
    </form>
</body>
</html>
