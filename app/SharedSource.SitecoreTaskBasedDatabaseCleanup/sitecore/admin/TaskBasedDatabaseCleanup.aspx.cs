﻿namespace SharedSource.SitecoreTaskBasedDatabaseCleanup.Sitecore.Admin
{
    using System;
    using System.Linq;
    using System.Web.UI.WebControls;

    using global::Sitecore.Configuration;

    public partial class TaskBasedDatabaseCleanup : System.Web.UI.Page
    {
        #region Fields

        private readonly CleanUpTaskRunner cleanUpTaskRunner;

        #endregion

        #region Constructors and Destructors

        public TaskBasedDatabaseCleanup()
        {
            this.cleanUpTaskRunner = new CleanUpTaskRunner();
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindDatabaseList();
                this.BindTaskList();
            }

            this.BindLog();
        }

        protected void RunOnClick(object sender, EventArgs e)
        {
            var selectedDatabases =
                (from ListItem databaseItem in this.databaseList.Items
                 where databaseItem.Selected
                 select databaseItem.Value).ToList();
            var selectedTasks =
                (from ListItem taskItem in this.taskList.Items where taskItem.Selected select taskItem.Value).ToList();

            var tasksToRun =
                selectedDatabases.SelectMany(
                    database =>
                    selectedTasks.Select(
                        task =>
                        new TaskToRun
                            {
                                Database = database, 
                                Task = (CleanUpTaskEnum)Enum.Parse(typeof(CleanUpTaskEnum), task)
                            }));

            this.cleanUpTaskRunner.RunCleanUp(tasksToRun);
            this.BindLog();
        }

        private void BindDatabaseList()
        {
            this.databaseList.DataSource = Factory.GetDatabaseNames();
            this.databaseList.DataBind();
        }

        private void BindLog()
        {
            this.log.Text = this.cleanUpTaskRunner.GetLog();
        }

        private void BindTaskList()
        {
            this.taskList.DataSource = CleanUpTaskRunner.TaskNames;
            this.taskList.DataBind();
        }

        #endregion
    }
}