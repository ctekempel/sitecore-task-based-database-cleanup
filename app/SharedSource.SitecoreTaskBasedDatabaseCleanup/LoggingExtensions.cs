namespace SharedSource.SitecoreTaskBasedDatabaseCleanup
{
    public static class LoggingExtensions
    {
        public static void RunLoggedAction<T>(this T @object, string description, System.Action<T> action, object caller)
        {
            global::Sitecore.Diagnostics.Log.Info(description + " started", caller);
            if (global::Sitecore.Context.Job != null)
            {
                global::Sitecore.Context.Job.Status.Messages.Add(description + " started");
            }

            action.Invoke(@object);
            global::Sitecore.Diagnostics.Log.Info(description + " finished", caller);
            if (global::Sitecore.Context.Job != null)
            {
                global::Sitecore.Context.Job.Status.Messages.Add(description + " finished");
            }
        }
    }
}