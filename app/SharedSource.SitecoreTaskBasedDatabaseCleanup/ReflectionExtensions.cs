namespace SharedSource.SitecoreTaskBasedDatabaseCleanup
{
    using System.Reflection;

    public static class ReflectionExtensions
    {
        public static T InvokePrivateMethod<T>(this object target, string methodName, params object[] parameters)
        {
            MethodInfo method = target.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(target, parameters);
            return (T)result;
        }        
        
        public static void InvokePrivateMethod(this object target, string methodName, params object[] parameters)
        {
            MethodInfo method = target.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(target, parameters);
        }
    }
}