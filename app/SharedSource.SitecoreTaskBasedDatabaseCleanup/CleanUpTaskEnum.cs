﻿namespace SharedSource.SitecoreTaskBasedDatabaseCleanup
{
    public enum CleanUpTaskEnum
    {
        CleanupCyclicDependences,
        CleanupInvalidLanguageData,
        CleanupFields,
        CleanupOrphans,
        CleanupBlobs,
        CleanupOrphanFields,
        RebuildDescendants
    }
}