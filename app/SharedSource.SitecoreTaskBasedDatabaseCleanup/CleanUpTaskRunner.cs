﻿namespace SharedSource.SitecoreTaskBasedDatabaseCleanup
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using global::Sitecore.Caching;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Data.DataProviders;

    using global::Sitecore.Jobs;

    public class CleanUpTaskRunner
    {
        #region Constants

        private const string JobName = "CleanUpTaskRunner";

        #endregion

        #region Public Properties

        public static IEnumerable<CleanUpTaskEnum> TaskNames
        {
            get
            {
                return new[]
                           {
                               CleanUpTaskEnum.CleanupCyclicDependences, CleanUpTaskEnum.CleanupInvalidLanguageData, 
                               CleanUpTaskEnum.CleanupFields, CleanUpTaskEnum.CleanupOrphans, CleanUpTaskEnum.CleanupBlobs, 
                               CleanUpTaskEnum.CleanupOrphanFields, CleanUpTaskEnum.RebuildDescendants
                           };
            }
        }

        #endregion

        #region Public Methods and Operators

        public string GetLog()
        {
            var job = JobManager.GetJob(JobName);

            if (job != null)
            {
                var logMessageBuilder = new StringBuilder();
                logMessageBuilder.AppendLine(string.Format("Status: {0}", job.Status.State));
                foreach (var message in job.Status.Messages)
                {
                    logMessageBuilder.AppendLine(message);
                }

                return logMessageBuilder.ToString();
            }

            return "Status: Not Running";
        }

        public void RunCleanUp(IEnumerable<TaskToRun> tasks)
        {
            var jobOptions = new JobOptions(
                JobName, 
                this.GetType().ToString(), 
                global::Sitecore.Context.Site.Name, 
                this, 
                "CleanUp", 
                new object[] { tasks });

            JobManager.Start(jobOptions);
        }

        #endregion

        // ReSharper disable once UnusedMember.Local
        #region Methods

        private void CleanUp(IEnumerable<TaskToRun> tasks)
        {
            foreach (var task in tasks)
            {
                task.RunLoggedAction(
                    string.Format("Clean Up Task Runner - {0} on {1}", task.Task, task.Database), 
                    taskToRun =>
                        {
                            var database = Factory.GetDatabase(taskToRun.Database);
                            foreach (var dataProvider in database.GetDataProviders())
                            {
                                switch (taskToRun.Task)
                                {
                                    case CleanUpTaskEnum.CleanupFields:
                                    case CleanUpTaskEnum.CleanupBlobs:
                                    case CleanUpTaskEnum.CleanupOrphanFields:
                                        dataProvider.InvokePrivateMethod(
                                            taskToRun.Task.ToString(), 
                                            new CallContext(
                                                new DataManager(database), 
                                                database.GetDataProviders().Count()));
                                        break;
                                    default:
                                        dataProvider.InvokePrivateMethod(taskToRun.Task.ToString());
                                        break;
                                }
                            }
                        }, 
                    this);
            }

            CacheManager.ClearAllCaches();
        }

        #endregion
    }
}