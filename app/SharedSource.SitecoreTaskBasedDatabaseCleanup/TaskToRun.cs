namespace SharedSource.SitecoreTaskBasedDatabaseCleanup
{
    public class TaskToRun
    {
        public string Database { get; set; }
        public CleanUpTaskEnum Task { get; set; }
    }
}